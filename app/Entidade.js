
class Entidade{

    constructor(nome, atributos){
        if(!nome) throw "Nome não pode estar em branco";
        if(!atributos) throw "Nome não pode estar em branco";

        this.$nome = nome;
        this.$atributos = atributos;
    }

    get nome(){ return this.$nome; }
    get atributos(){ return this.$atributos; }
}

module.exports = Entidade;