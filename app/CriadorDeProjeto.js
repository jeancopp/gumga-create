var CriadorAbstract = require("./CriadorAbstract");

const NOME_DO_PROJETO_INDICE = 3;
const PASTA_INDICE = 4;
const VERSAO_INDICE = 5;
const PACOTE_BASE_INDICE = 6;

class CriadorDeProjeto extends CriadorAbstract {
 
    executar(parametros) {
        var versao = this.lerVersao(parametros);
        var pacote = this.lerPacote(parametros);
        var artefato = this.lerArtefato(parametros);
        var pasta = this.lerPasta(parametros);
        
        console.log("Criando o projeto");
        this.$script.executarCriacaoDeProjeto(pasta, pacote, artefato, versao);
    }
    lerArtefato(parametros) {
        return parametros[NOME_DO_PROJETO_INDICE];
    }
    lerPacote(parametros) { 
        return parametros[PACOTE_BASE_INDICE];
    }
    lerVersao(parametros) { 
        return parametros[VERSAO_INDICE];
    };
    lerPasta(parametros) {  
        return parametros[PASTA_INDICE];
    };
}

module.exports = CriadorDeProjeto;