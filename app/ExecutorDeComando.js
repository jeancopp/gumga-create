var exec = require('child_process').execSync;
module.exports = function (cmd) {
    try {
        var ret = exec(cmd);

        if (ret.error) {
            console.log("Erro ao executar:", cmd, ret.error)
            throw error;
        }

        if (global.exibeLog) {
            console.log(ret.stdout ? ret.stdout : ret.toString("utf-8"));
            if (ret.stderr) console.log(ret.stderr);
            if (ret.status) console.log(ret.status);
        }
    } catch (e) {
        console.debug(e);
        throw e;
    }
};