class ExecutaScript{

    constructor(executorDeComando){
        this.executarComando = executorDeComando;
    }

    executarScriptGumga(pastaBase, pastaFilha, script){
        var nomeDaPasta = pastaBase.split(/\\|\//).filter( s => s.length ).pop();
        var cmd = "cd "+pastaBase+" && cd "+nomeDaPasta+pastaFilha+" && " +script;
        this.executarComando(cmd);
    }

    executarInstalacaoMvn(pastaBase){
        this.executarComando("cd "+pastaBase+" && mvn install");
    }

    executarCriacaoDeProjeto(pasta, pacote,artefato,versao){
        this.executarComando("cd "+pasta+" && mvn archetype:generate "+
        "-DinteractiveMode=false "+      
        "-DarchetypeGroupId=io.gumga "+
        "-DarchetypeArtifactId=gumga-archetype "+
        "-DarchetypeVersion=LATEST "+
        "-DgroupId="+pacote+" "+      
        "-DartifactId="+artefato+" "+
        "-Dversion="+versao);
    }

}

module.exports = ExecutaScript;