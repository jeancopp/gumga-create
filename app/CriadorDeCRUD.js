
const retornarComandoDeDominio = entidade => 'mvn io.gumga:gumgag:entidade -Dentidade=' + entidade.nome
    + ' -Datributos="' + entidade.atributos + '"';
const retornarComandoDeAplicacao = entidade => 'mvn io.gumga:gumgag:aplicacao -Dentidade=' + entidade.nome;
const retornarComandoDeApi = entidade => 'mvn io.gumga:gumgag:api -Dentidade=' + entidade.nome;
const retornarComandoDeApresentacao = entidade => 'mvn io.gumga:gumgag:apresentacao -Dentidade=' + entidade.nome;

var CriadorAbstract = require("./CriadorAbstract");

class CriadorDeCRUD extends CriadorAbstract {

    executar(parametros) {
        var entidades = this.lerEntidades(parametros);
        var pasta = this.lerPasta(parametros);
        try {
            console.log("Criando os domínios");
            this.criarDomains(entidades, pasta);

            console.log("Instalando a aplicação no repositório maven");
            this.instalarAplicacao(pasta);

            console.log("Criando as applications");
            this.criarApplications(entidades, pasta);

            console.log("Criando as APIs");
            this.criarApis();
            
            console.log("Criando as presentations");
            this.criarPresentations(entidades, pasta);
        } catch (e) {
            throw e;
        }
    }

    incrementarEntidades(entidades, pasta, callback) {
        for (var ind in entidades) {
            var entidade = entidades[ind];
            callback(this.$script, pasta, entidade);
        }
    }
    instalarAplicacao(pasta) {
        this.$script.executarInstalacaoMvn(pasta);
    }

    criarDomains(entidades, pasta) {
        this.incrementarEntidades(entidades, pasta,
            (s, p, e) => s.executarScriptGumga(p, "-domain", retornarComandoDeDominio(e)));
    }
    criarApplications(entidades, pasta) {
        this.incrementarEntidades(entidades, pasta,
            (s, p, e) => s.executarScriptGumga(p, "-application", retornarComandoDeAplicacao(e)));
    }
    criarApis(entidades, pasta) {
        this.incrementarEntidades(entidades, pasta,
            (s, p, e) => s.executarScriptGumga(p, "-api", retornarComandoDeApi(e)));
    }
    criarPresentations(entidades, pasta) {
        this.incrementarEntidades(entidades, pasta,
            (s, p, e) => s.executarScriptGumga(p, "-presentation", retornarComandoDeApresentacao(e)));
    }

    lerEntidades(parametros) {
        var Entidade = require('./Entidade');

        const INICIO_INDICE = 4;
        var entidades = [];

        for (var ind = INICIO_INDICE; ind < parametros.length; ind += 2) {
            entidades.push(new Entidade(parametros[ind], parametros[ind + 1]));
        }
        return entidades;
    }

    lerPasta(parametros) {
        const PASTA_INDICE = 3;
        var pasta = parametros[PASTA_INDICE];
        return pasta;
    }
}

module.exports = CriadorDeCRUD;