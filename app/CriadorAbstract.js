const ExecutaScript = require("./ExecutaScript");

class CriadorAbstract{
    constructor(){
        var executorDeComandos = require(global.ExecutorDeComando);
        this.$script = new ExecutaScript(executorDeComandos);
    }
    get script(){ return this.$script; }
  
}
module.exports = CriadorAbstract;