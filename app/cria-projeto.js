#!/usr/bin/env node
var CriadorDeCRUD = require('./CriadorDeProjeto');
var Entidade = require('./Entidade');

const PASTA_INDICE = 2;
const INICIO_INDICE = 3;

if( !process.argv || !process.argv.length || process.argv.length < 4 || ( process.argv.length - 3) % 2 !== 0 ){
    var saida = "";
    require("./package.json").mensagem.erro.map( s => saida+=s+"\n").toString();
    throw saida;
}

var entidades = [];
var pasta = process.argv[PASTA_INDICE];
for(var ind = INICIO_INDICE; ind < process.argv.length; ind+=2 ){
    entidades.push(new Entidade(process.argv[ind], process.argv[ind+1]));
}

var criador = new CriadorDeCRUD(entidades, pasta);

try{
    console.log("Criando os domínios");
    criador.criarDomains();

    console.log("Instalando a aplicação no repositório maven");
    criador.instalarAplicacao();

    console.log("Criando as applications");
    criador.criarApplications();
    console.log("Criando as APIs");
    criador.criarApis();
    console.log("Criando as presentations");
    criador.criarPresentations();
}catch(e){
    throw e;
}



  