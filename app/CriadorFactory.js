const INDICE_DE_CRIADOR = 2;

module.exports.fabricar = function(parametros){
    if(parametros[INDICE_DE_CRIADOR].toUpperCase() === "PROJETO"){
        var CriadorDeProjeto = require("./CriadorDeProjeto");
        return new CriadorDeProjeto();
    }else if(parametros[INDICE_DE_CRIADOR].toUpperCase() === "CRUD"){
        var CriadorDeCRUD = require("./CriadorDeCRUD");
        return new CriadorDeCRUD();
    }else {
        throw "Opcao invalida!";
    }
};