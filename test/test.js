var factory = require("./app/CriadorFactory");
var assert = require('assert');

describe('Array', function () {
  describe('#indexOf()', function () {
    it('should return -1 when the value is not present', function (test) {
      global.ExecutorDeComando = "";
      global.mockTest = test;
      var ret = factory.fabricar(["", "", "CRUD"]);
      assert.equal(-1, [1, 2, 3].indexOf(4));
    });
  });
});