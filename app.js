#!/usr/bin/env node

var Validador = require("./app/Validador");
if( Validador.entradaInvalida(process) ){
    var saida = "";
    require("./package.json").mensagem.erro.map( s => saida+=s+"\n").toString();
    throw saida;
}

global.ExecutorDeComando = __dirname + "/app/ExecutorDeComando.js";
global.exibeLog = false;

var factory = require("./app/CriadorFactory");
var executor = factory.fabricar(process.argv);

executor.executar(process.argv);

console.log('Criação Concluída!');